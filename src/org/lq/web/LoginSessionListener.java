package org.lq.web;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

import org.lq.entity.Users;

/**
 * Application Lifecycle Listener implementation class LoginSessionListener
 * 单点登录
 */
@WebListener
public class LoginSessionListener implements HttpSessionAttributeListener {
	//存储已经登录的所有用户 key = 用户名 value = 存储当前登录用户的session对象
    Map<String,HttpSession> map = new HashMap<String,HttpSession>();
    
	/**
     * @see HttpSessionAttributeListener#attributeAdded(HttpSessionBindingEvent)
     */
    public void attributeAdded(HttpSessionBindingEvent se)  { 
    	//获取当前setAttribute的key
    	String key = se.getName();
    	//判断是否是登录的操作
    	if("user".equals(key)) {
    		//获取存储的value(登录的用户对象)
    		Users user= (Users) se.getValue();
    		//判断之前是否有已经登录的用户
    		if(map.get(user.getName()) != null) {
    			//如果map中存在,代表该账户已经被登录过了!
    			HttpSession oldSession = map.get(user.getName());
    			//获取之前登录的用户对象
    			Users oldUser= (Users) oldSession.getAttribute("user");
    			//强制下线(销毁session)
    			oldSession.invalidate();
    			System.out.println("账户 : "+oldUser.getName()+"在"+oldUser.getIp());
    		}
    		//将Session已用户名为索引,存入到map中
    		map.put(user.getName(), se.getSession());
    		System.out.println("账户 : "+user.getName()+"在"+user.getIp());
    	}
    	
    }

	/**
     * @see HttpSessionAttributeListener#attributeRemoved(HttpSessionBindingEvent)
     */
    public void attributeRemoved(HttpSessionBindingEvent se)  { 
         // TODO Auto-generated method stub
    }

	/**
     * @see HttpSessionAttributeListener#attributeReplaced(HttpSessionBindingEvent)
     */
    public void attributeReplaced(HttpSessionBindingEvent se)  { 
    	//获取修改的key
    	String key = se.getName();
    	if("user".equals(key)) {
    		//获取没有注销情况下,用另一个账号登录的信息(只有第一次set的时候才会赋值)
    		//获取user key第一次set赋值的对象
    		Users oldUser = (Users) se.getValue();
    		//先把之前存储的session从map中移除
    		map.remove(oldUser.getName());
    		//获取最新添加进来的用户对象
    		Users newUser= (Users) se.getSession().getAttribute("user");
    		//如果新添加的用户的和map中有相同的
    		if(map.get(newUser.getName()) != null) {
    			//获取之前的session
    			HttpSession session = map.get(newUser.getName());
    			//销毁session
    			session.invalidate();
    			System.out.println("你的账号被强制下线了!!");
    		}
    		//将最新的用户信息添加到map中
    		map.put(newUser.getName(),se.getSession());
    	}
    			
    }
	
}
