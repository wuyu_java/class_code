package org.lq.web;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.lq.entity.Users;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Users u = new Users();
		Users u2 = new Users();
		u2.setName("wuyu");
		
		String uname = request.getParameter("uname");
		String upwd = request.getParameter("upwd");
		
		if(u.getName().equals(uname) && u.getPwd().equals(upwd)) {
			u.setIp(request.getRemoteAddr());
			u.setLoginTime(new Date());
			request.getSession().setAttribute("user", u);
			response.sendRedirect("main.jsp");
		}else if(u2.getName().equals(uname) && u2.getPwd().equals(upwd)) {
			u2.setIp(request.getRemoteAddr());
			u2.setLoginTime(new Date());
			request.getSession().setAttribute("user", u2);
			response.sendRedirect("main.jsp");
		}else {
			response.sendRedirect("login.jsp");
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
